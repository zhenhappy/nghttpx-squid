FROM alpine:edge

ADD entrypoint.sh /entrypoint.sh

RUN apk add --no-cache --update squid nghttp2

ENTRYPOINT sh /entrypoint.sh

EXPOSE 8443
