# nghttpx-squid

## 使用方法

### 编译
```
docker image build -t nghttpx-squid .
```

### 配置
在config目录下放入证书和秘钥, 分别改名为server.crt和server.key

### 运行
```
docker container run --restart=always -v /root/nghttpx-squid/config:/config -it nghttpx-squid --name 'nghttpx-squid'
```
